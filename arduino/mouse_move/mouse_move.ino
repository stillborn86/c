/*
20141216 - Created by stillborn86 (c)

This sketch utilizes a push-button in order to start/stop random mouse motion to
create a proof of concept for the Leonardo functionality.
*/

int buttonPin = 2;

long x;
long y;

int xSign;
int ySign;

int buttonState = 0;

void setup()
{
  pinMode(buttonPin,INPUT);
  Mouse.begin();
}

void loop()
{
  x = random(1,30);
  y = random(1,30);
    
  xSign = random(1,3);
  ySign = random(1,3);
  
  if (xSign >= 2)
  {
    x = -x;
  }
  if (ySign >= 2)
  {
    y = -y;
  }
  
  buttonState = digitalRead(buttonPin);
  
  if (buttonState == HIGH)
  {
    Mouse.move((float)x,(float)y);
    delay(300);
  }
}
