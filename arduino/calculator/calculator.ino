/*
   20161201 - Created by stillborn86 (c)

   This is a simple calculator script, designed to exercise the use of functions
   and passing variables between functions.
*/

void setup(){
    Serial.begin( 115200 );
}

void displayMenu( int& choice ){
    bool correct = false;

    while( !correct ){
        Serial.println( "\nPlease choose an operation to perform" );
        Serial.println( "1. Add\r2. Subtract\n3. Multiply\n4. Divide" );
        Serial.println( "?: " );

        while( !Serial.available() );
        choice = Serial.parseInt();
        Serial.println( choice );

        if( choice < 1 || choice > 4 ){
            Serial.println( "\n\tYour choice is bad, and you should feel bad!");
            correct = false;
        }
        else
            correct = true;
    }

}

double addNum( double x, double y ){
    return x + y;
}

double subtractNum( double x, double y ){
    return x - y;
}

double multiplyNum( double x, double y ){
    return x * y;
}

double divideNum( double x, double y ){
    return x / y;
}

void getVariables( double& x, double& y ){
    Serial.println( "\n\nPlease input your first number: " );
    while( !Serial.available() );
    x = Serial.parseFloat();
    Serial.println( x );

    Serial.println( "\nPlease input your second number: ");
    while( !Serial.available() );
    y = Serial.parseFloat();
    Serial.println( y );
}

double doMath( int choice, int a, int b ){
    if( choice == 1 )
        return addNum( a, b );
    else if( choice == 2 )
        return subtractNum( a, b );
    else if( choice == 3 )
        return multiplyNum( a, b );
    else
        return divideNum( a, b );
}

void displayAnswer( double ans ){
    Serial.println( "\n\nThe answer is: " );
    Serial.println( ans );
}


void loop(){
    int choiceNum;
    choiceNum = 0;
    displayMenu( choiceNum );

    double firstNum, secondNum;
    getVariables( firstNum, secondNum );
    double answer = doMath( choiceNum, firstNum, secondNum );
    displayAnswer( answer );
}


