/*
20141128 - Created by stillborn86 (c)

This sketch lights up a random LED on ports 2, 3, 4, or 5 when a button is
pressed.
*/

int pinOn;
int randX;
int switchPin = 8;

void setup()
{
  pinMode(2, OUTPUT);  // Initializes each pin as an output
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(switchPin, INPUT);
  
  int pinOn = random(2,6);  // Pick a random GPIO
  digitalWrite(pinOn, HIGH);  // Turn GPIO on
}

boolean debounce(boolean last)
{
  boolean current = digitalRead(switchPin);
  if (last != current)
  {
    delay(5);
    current = digitalRead(switchPin);
  }
  return current;
}

void loop()
{
 // Wait for button to be pushed
}
