/*
20141222 - Created by stillborn86 (c)

A typical SIMON game using four LEDs, four push-buttons, and a pezio speaker.
The player is given half a second between button presses, which is decreased
every five turns.

After 30 rounds (35 total lights), the player wins.  If the player presses an
incorrect button before that, the player loses.
*/

byte done = 1;  // Variable done is 1 as long as game is still playing, 0 when
                // game is won or lost

byte ledCount = 4;  // Number of LEDs, buttons, and buzzers

byte speakerPin = 0;  // Declares which pin the speaker is connected to
                      // This speaker is connected to a 100ohm resistor

byte durationDenom = 1;  // Denominator each duration will be divided by,
                         // increasing every five rounds, shortening each turn
                         // timer
int duration = 300;  // Duration each light/beep is on
                     // divided by durationDenom
int timerClick = 500;  // Amount of time allotted between each button press
                       // before player loses -- divided by durationDenom

// Determines the random order of lights and beeps for a sequence of 35 total
// lights
int key[] = { random(1,5), random(1,5), random(1,5), random(1,5), random(1,5),
random(1,5), random(1,5), random(1,5), random(1,5), random(1,5), random(1,5),
random(1,5), random(1,5), random(1,5), random(1,5), random(1,5), random(1,5),
random(1,5), random(1,5), random(1,5), random(1,5), random(1,5), random(1,5),
random(1,5), random(1,5), random(1,5), random(1,5), random(1,5), random(1,5),
random(1,5), random(1,5), random(1,5), random(1,5), random(1,5), random(1,5) };
byte turnCount = 4;  // Initiates the variable that counts the number of
                     // successful rounds, starting with 5 lights for the first
                     // round

                //G   R  B  Y
int ledPins[] = { 12, 9, 6, 3 };  // Declares which pins are connected to the
                                  // LEDs

                 //G   R   B  Y
int buttPins[] = { 13, 10, 7, 4 };  // Declares which pins are connected to the
                                    // buttons

                //G   R   B   Y
int Pitches[] = { 41, 55, 82, 69 };  // Declares which pitches occur with each
                                     // light
                //E1  A1  E2  CS2

void setup()
{
	for (byte x = 0; x < ledCount; x++)  // Sets each LED pin as an output
	{
		pinMode(ledPins[x], OUTPUT);
	}
  
	for (byte x = 0; x < ledCount; x++)  // Sets each button pin as an input
	{
		pinMode(buttPins[x], INPUT);
	}
  
	pinMode(speakerPin, OUTPUT);  // Sets the speaker pin as an output
}

void loop()
{
	while(done == 1)  // While the game is running
	{
	
		if (turnCount == 10 || turnCount == 15 || turnCount == 20 ||
        turnCount == 25 || turnCount == 30)  // Increments durationDenom every
                                             // five rounds, speeding up each
                                             // round
		{
			++durationDenom;
		}
 
		float timer = timerClick / durationDenom;  // Creates a new time limit
                                                   // between clicks
 		float ledBlink = duration / durationDenom;  // Creates a new blink speed
                                                    // during play
 
 		for (byte play = 0; play < turnCount; play++)  // Blinks through
                                                       // sequence up to the max
                                                       // for this round
 		{
		 	digitalWrite(ledPins[key[play]], HIGH);  // Turns the LED for at
                                                     // this point of the
                                                     // sequence on
 			tone(speakerPin, Pitches[key[play]], ledBlink);  // Plays tone for
                                                             // specific LED
                                                             // color, for
                                                             // predetermined
                                                             // amount of time
 			digitalWrite(ledPins[key[play]], LOW);  // Turns the LED off
 			delay(ledBlink);  // Pauses between each blink, so player isn't
                              // overwhelmed
		}
 
 		for (byte play = 0; play < turnCount; play++)  // Player's turn cycles
 		{
 			byte buttonPress = 0;  // Initiates which button press the player is
                                   // on, starting with 0 as the first button
 	
 			while(buttonPress ==0)  // As long as the player doesn't press a
                                    // button
 			{
 				--timer;  // Decrease the timer variable by one
 		
 				if (digitalRead(13) == HIGH)  // If player presses the green
                                              // button
 				{buttonPress = 1;}
 				if (digitalRead(10) == HIGH)  // If player presses the red
                                              // button
 				{buttonPress = 2;}
 				if (digitalRead(7) == HIGH)  // If player presses the blue
                                             // button
 				{buttonPress = 3;}
 				if (digitalRead(4) == HIGH)  // If player presses the yellow
                                             // button
 				{buttonPress = 4;}
 		
 				digitalWrite(ledPins[buttonPress-1], HIGH);  // Turns selected
                                                             // LED on
 				tone(speakerPin, Pitches[buttonPress-1], ledBlink);  // Plays
                                                                     // tone for
                                                                     // selected
                                                                     // LED for
                                                                     // a period
                                                                     // of time
 				digitalWrite(ledPins[buttonPress-1], LOW);  // Turns selected
                                                            // LED off
 			}
 	
 			if (buttonPress == key[play])  // If the correct button is pressed
                                           // for this part of the turn
 			{
 				
 				
 				
 				
 				
 				
 				
 				
 				
 				
 				
 				
 				
 				
 				
 				
 				
 				
 				++turnCount;  // Increments to the next turn
 			}
 	
 			else  // If the wrong button was pressed for this part of the turn
 			{
 				delay(30);
 			for (byte x = 0; x < 5; x++)  // Flashes "wrong" LED 5 times
 			{
 				digitalWrite(ledPins[buttonPress-1], HIGH);  // Turn "losing"
                                                             // LED on
 				tone(speakerPin, 3951, 100);  // Plays tone for 0.1 seconds
 						   //B7
 				digitalWrite(ledPins[buttonPress-1], LOW);  // Turn "losing" LED
                                                            // off
 			}
 		
 			done = 0;  // Ends void loop() cycle
 			}
 		}

  		if (turnCount == 35)  // You win!!!
  		{
    		delay(80);  // Wait 0.08 seconds before playing victory tone
    
    		for (byte x = 0; x < 5; x++)  // Flashes all LEDs and beeps 6 times
	 		{
      			for (byte y = 0; y < ledCount; y++)  // Turn all of the LEDs on
    		 	{
        			digitalWrite(ledPins[y], HIGH);
      			}
      
      			tone(speakerPin, 31, 70);  // Winning tone is played for 0.07
                                           // seconds
                    		   //B0
      
      			for(byte y = 0; y < ledCount; y++)  // Turn all of the LEDs off
      			{
       				digitalWrite(ledPins[y], LOW);
      			}
      
      			delay(20);  // Leave LEDs off for 0.02 seconds
    		}
    		done = 0;  // Ends void loop() cycle
  		}
	}
}
