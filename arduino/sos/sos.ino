/*
20141204 - Created by stillborn86 (c)

This sketch blinks an LED in an SOS pattern.

This blinking only occurs when it is low light, to conserve battery power to the
device.
*/

int dark = 100;  // Declares the darkness level before the LED starts blinking

int sensePin = 0;  // Declares the GPIO for the photoresistor and LED
int ledPin = 13;

int blinkNum = 0;  // Sets the system to start from the first blink
int blinkDelay = 0;  // Initiates the delay between blinks

void setup()
{
  pinMode(ledPin, OUTPUT);  // Sets the LED GPIO as an output
}

void loop()
{
  int val = analogRead(sensePin);  // Reads the photoresistor and stores its
                                   // value into a variable
  
  if (val <= dark)  // If the photoresistor reads a level below the darkness
                    // threshold
  {
    int val = analogRead(sensePin);  // Re-reads the photoresistor within the IF
                                     // loop
    
    if (blinkNum == 0 || blinkNum == 1 || blinkNum == 2 || blinkNum == 6 ||
    blinkNum == 7 || blinkNum == 8)  // If a "fast" blink is supposed to occur
    {
      blinkDelay = 200;
    }
    if (blinkNum == 3 || blinkNum == 4 || blinkNum == 5)  // If a "long" blink
                                                          // is supposed to
                                                          // occur
    {
      blinkDelay = 400;
    }
    
    digitalWrite(ledPin, HIGH);  // Turns on the LED
    delay(blinkDelay);
    digitalWrite(ledPin, LOW);  // Turns off the LED
    delay(blinkDelay);
    
    if (blinkNum == 2 || blinkNum == 5 || blinkNum == 8)  // Pauses between
                                                          // "letters"
    {
      delay(500);
    }
    
    blinkNum = blinkNum + 1;  // Increments the blink number counter thingy
    
    if (blinkNum >= 9)  // After last "letter" the system resets the counter and
                        // waits to start the next "word"
    {
      delay(300);
      blinkNum = 0;
    }
  }
  else
  {
    blinkNum = 0;   // The blink counter is reset if light is turned on
  }
}
