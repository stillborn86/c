/*
20141210 - Created by stillborn86 (c)

This sketch begins buzzing a pezio electric buzzer at a given tone, determined
by the amount of ambient light exposed to a reversed LED or photoresistor.
*/

int sensePin = 0;  // Sets the GPIO for the analog photoresistor

int Buzzer = 3;  // Sets the GPIO for the pezio buzzer

void setup()
{
  pinMode(Buzzer, OUTPUT);  // Sets each of the LED GPIO pins as outputs
}

void loop()
{
  int val = analogRead(sensePin) - 25;  // Picks up the analog value from the
                                        // photoresistor
  
  analogWrite(Buzzer, val);  // Sets the tone of the pezio buzzer to the output
                             // of the photoresistor
}

