/*
20151214 - Created by stillborn86 (c)

This sketch is designed to test serial input and output between the user and the
ARDUINO platform trough the serial monitor (115200 BAUD).  The user is prompted
for a number between 1 and 4.  The sketch will then wait for the user's input.
If the user provides a correct number, the sketch will repeat the input, and
repeat from the beginning.  If the user provides a bad response, the sketch will
alert the user to the poor choice.
*/

byte selection;

void setup(){
    Serial.begin(115200);  // Start the serial monitor at 115200 BAUD
    Serial.println("Please make a selection (1-4): ");  // Prompt the user for a
                                                        // selection
}

void loop(){
   if (Serial.available() != 0){  // Wait for the user's input
        selection = Serial.parseInt();  // Get input
        change(selection);  // Pass user's input to change function

        Serial.println("\nPlease make another selection (1-4): ");  // Prompt
                                                                    // user for
                                                                    // a new
                                                                    // selection
    }
}

void change(int number){
    if (number != 1 && number != 2 && number != 3 && number != 4){  // Assure
                                                                    // user's
                                                                    // choice is
                                                                    // good
        Serial.println("Your choice is bad and you should feel bad!");
    }
    else{
        Serial.print("Your choice is: ");  // Print user's good choice
        Serial.println(number);
    }  
}

