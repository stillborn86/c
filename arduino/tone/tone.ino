const byte spPin = 11;

// Watchdog interrupt
ISR( WDT_vect){
}

void setup(){
    // Set all pins as outputs, to save a few mA
    for( byte i = 0; i < 20; i++ ){
        pinMode( i, OUTPUT );
    }
        
    // Set up watchdog timer
    WDTCSR = ( 24 );  // Change enable and WDE - also resets
    WDTCSR = ( 33 );  // Prescalers only - get rid of the WDE and WDCE bit
    WDTCSR |= ( 1 << 6 );  // Enable interrupt mode

    ADCSRA &= ~( 1 << 7 );  // Disable ADC
    SMCR |= ( 1 << 2 );  // Set power down mode
    SMCR |= 1; // Enable sleep

    // Run through some tones, to assure we're working
    tone( spPin, 2500, 800 );
    delay( 200 );
    tone( spPin, 500, 800 );
    delay( 200 );
    tone( spPin, 2500, 800 );
    delay( 200 );
}

void loop(){
    byte beeps = random( 1, 3 );  // Pick a random number of beeps
    int duration = random( 50, 200 );  // Get a random duration for our beeps
    byte wait = random( 7, 30 );  // Pick a multiple of 8s to wait to repeat
                      // Between 2 minutes and 5 minutes of delay

    for( int i = 0; i <= beeps; i++ ){
        tone( spPin, random( 31, 4000 ), duration );
        delay( random( 2, 2000 ) / 4 );
    }

    noTone( spPin );

    for( int i = 0; i < wait; i++ ){  // Wait 8 seconds at a time
        //Disable BOD
        MCUCR |= ( 3 << 5 );  // Set both BODS and BODSE at the same time
        MCUCR = ( MCUCR & ~( 1 << 5 ) ) | ( 1 << 6 );  // Then set the BODS bit
                                                       // to clear the BODSE at
                                                       // the same time

        __asm__  __volatile__( "sleep" );
    }
}

