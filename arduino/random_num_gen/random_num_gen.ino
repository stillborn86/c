/*
20141128 - Created by stillborn86 (c)

This sketch generates a random number between 1 and 10 every 1/2 second.
*/

int randX;
int x = 1;
int y = 11;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  int randX = random(x,y);
  Serial.println(randX);
  delay(500);
}
