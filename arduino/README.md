# ARDUINO sketch compilation
This directory is full of my Arduino sketches designed for the Arduino platform,
including the Nano, Uno, and the Leonardo.  These platforms are largely
interchangeable, so there is no indication which sketches were written for which
board.

For this reason, special attention must be paid to which pins are assigned for
which functions.

Nano
![Arduino NANO](http://arduino.cc/en/uploads/Main/ArduinoNanoFront_3_lg.jpg)

Uno
![Arduino UNO](http://arduino.cc/en/uploads/Main/ArduinoUno_R3_Front.jpg)

Leonardo
![Arduino LEONARDO](http://arduino.cc/en/uploads/Main/ArduinoLeonardoFront_2.jpg)

## Tutorial folder
The tutorial folder is full of sketches that were either designed by others or
created with the help of others.  It was designed to be used as a helpful tool
for writing sketches in the future.

## Image files
The images are references, including GPIO pinout diagrams, and microcontroller
pinout diagrams.

Nano GPIO pinout
![NANO Pinout](https://bytebucket.org/stillborn86/c/raw/d93c05978d10573e90c45fe2b9beca2fd726644c/arduino/nano_pinout_2.jpg?token=9edc03555c643bc5a089ddba51ebfb9667360eb1)

Uno GPIO pinout
![UNO Pinout](https://bytebucket.org/stillborn86/c/raw/d93c05978d10573e90c45fe2b9beca2fd726644c/arduino/uno_pinout.png?token=aae6dc9fff9a03d1f23e868de9311781268419b2)

Leonardo GPIO pinout
![LEONARDO Pinout](https://bytebucket.org/stillborn86/c/raw/d93c05978d10573e90c45fe2b9beca2fd726644c/arduino/leonardo_pinout_1.png?token=d667bad6567ae4e053d9444fc9e1acbefc7422b9)

ATMega chipset
![ATMega Chipset](https://bytebucket.org/stillborn86/c/raw/d93c05978d10573e90c45fe2b9beca2fd726644c/arduino/atmega328_pinout_2.png?token=334f9d6ad308109484b661fa44ed5561e94fa138)
