/*
My first program (20141128)

This causes the on-board LED to blink every second
*/


int ledPin = 13;

// Code that will run during setup ONLY
void setup()
{
  // Initialize pins as outputs
  pinMode(ledPin, OUTPUT);
}

// Code that will run, FOREVER, once the setup is complete
void loop()
{
  // Turn LED on
  digitalWrite(ledPin, HIGH);
  
  // Wait one second (in milliseconds)
  delay(1000);
  
  // Turn LED off
  digitalWrite(ledPin, LOW);
  
  // Wait one second
  delay(1000);
}
