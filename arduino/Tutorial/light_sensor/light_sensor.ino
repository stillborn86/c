/*

*/

int sensePin = 0;
int ledPin = 9;

void setup()
{
  analogReference(DEFAULT);  // Isn't necessary as it will be done by default
  pinMode(ledPin, OUTPUT);
}

void loop()
{
  int val = analogRead(sensePin);
  
  val = constrain(val, 750, 900);  // Steps the brightness of the LED as
                                   // darkness is stepped up
  int ledLevel = map(val, 750, 900, 255, 0);
  
  analogWrite(ledPin, ledLevel);
}
