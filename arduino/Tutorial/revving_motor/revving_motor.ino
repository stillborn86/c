/*
20141128

This program revs a motor up to a top speed, holds it at that speed for a period
of time, downrevs the motor back to zero, holds it there for a period of time,
and repeats... effectively pulsating a DC motor.
*/

void setup()
{
  pinMode (motorPin, OUTPUT);
}

void loop()
{
 // Accelrate the motor from 0 -> 255
 for (int i=0; i<=255; i++)
 {
   analogWrite(motorPin, i);
   delay(10);
 }
 
 // Hold at top speed
 delay(500);
 
 // Decrease speed from 255 -> 0
 for (int i=255; i>=255; i--)
 {
  analogWrite(motorPin, i);
  delay(10);
 }
 
 // Hold at zero
 delay(500);
}
