/*
20141222 - Created by stillborn86 (c)

This is an even smaller version of the SOS sketch utilizing arrays.
*/

const byte ledPin = 13;

const int blinkNum[] = { 175, 175, 175, 325, 325, 325, 175, 175, 175 };
const byte blinkCount = 9;

void setup()
{
 pinMode(ledPin, OUTPUT);
}

void loop()
{
 for(byte x = 0; x < blinkCount; ++x)
  {
    digitalWrite(ledPin, HIGH);
    delay(blinkNum[x]);
    digitalWrite(ledPin, LOW);
    
    if (x != 2 && x != 5)
    {
      delay(blinkNum[x]);
    }
    else
    {
      delay(400);
    }
  } 
delay(700);
}
