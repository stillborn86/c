/*
Created by stillborn86 (c)
*/

int x;
int y;

int xDir = 0;
int yDir = 0;

void setup()
{
    Mouse.begin();
}

void loop()
{
    x = random(1,50);
    y = random(1,50);
    
    xDir = random(1,3);
    yDir = random(1,3);

    if (xDir == 1)
    {
        x = -x;
    }
    
    if (yDir == 1)
    {
        y = -y;
    }
    
    Mouse.move(x,y);
    
    delay(150);
}
