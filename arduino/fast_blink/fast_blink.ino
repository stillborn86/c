  void setup() {
  // Set Pin13 as an output for LED use
  pinMode (13, OUTPUT);
  
  // Turn off all interrupts via ATMega328p datasheet
  // Avoids "jittery" interruptions for square wave
  TIMSK0 = 0;
}

void loop() {
  // Turn Pin13 on and off using Arduino stock code
  // digitalWrite (13, HIGH);
  // digitalWrite (13, LOW);
  
  // Turn PIN13 on and off using binary code for PB5 on ATMega328
  // B00000000 is binary for an 8-bit port
  PORTB = B00100000;
  PORTB = B00000000;

}
