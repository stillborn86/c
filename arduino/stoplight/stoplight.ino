/*
20141128 - Created by stillborn86 (c)

This program simulates the red, yellow, green cycle of a traffic light.
*/

int ledGreen = 2;
int ledYellow = 3;
int ledRed = 4;

void setup()
{
  // Set up GPIO as outputs
  pinMode(ledGreen, OUTPUT);
  pinMode(ledYellow, OUTPUT);
  pinMode(ledRed, OUTPUT);
}

void loop()
{
  // Turn turn on red light
  digitalWrite(ledRed, HIGH);
  
  // Delay for 2 sec
  delay(2000);
  
  // Turn off red light and turn on green light
  digitalWrite(ledRed, LOW);
  digitalWrite(ledGreen, HIGH);
  
  // Delay for 2 sec
  delay(2000);
  
  // Turn off green light and turn on yellow light
  digitalWrite(ledGreen, LOW);
  digitalWrite(ledYellow, HIGH);
  
  // Delay for 0.5 sec and turn of yellow light
  delay(500);
  digitalWrite(ledYellow, LOW);
}
