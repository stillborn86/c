/*
20141222 - Created by stillborn86 (c)

This is an even smaller version of the SOS sketch utilizing arrays.
*/

const byte ledPin = 13;

void setup(){
    pinMode(ledPin, OUTPUT);
}

void loop(){
    int delayTime;

    for( byte x = 0; x < 9; x++ ){
        if( x == 0 || x == 1 || x == 2 || x == 6 || x == 7 || x == 8 )
            delayTime = 175;
        else
            delayTime = 325;

        digitalWrite(ledPin, HIGH);
        delay(delayTime);
        digitalWrite(ledPin, LOW);
    
        if (x != 2 && x != 5)
          delay(delayTime);
        else
          delay(400);
    }

    delay(700);
}
