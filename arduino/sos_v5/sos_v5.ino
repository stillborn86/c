/*
 * 20190730 - Created by stillborn86 (c)
 * 
 * This is a version of the SOS sketch, utilizing
 * switch/case statements to run the LED blink
 * in parallel with any other code.
 * 
 */

#define ledPin 13

void setup(){
    pinMode( ledPin, OUTPUT );
}

void loop(){
    sos();
}

void sos(){
    static unsigned long ourTime;                         // Keep track of our "delays" with a millisecond timer
    static byte letterCount = 0,                          // Keep track of how many "pulses" we have displayed for each letter
                round = 1,                                // Keep track of if this is our first or second "S"
                position = 1;                             // Move us through each step of LED on, delay, LED off, delay, etc.
    
    switch( position ){
        // Begin letter "S"
        case 1:
            digitalWrite( ledPin, HIGH );
            ourTime = millis();
            letterCount++;
            position = 2;
            break;
        case 2:
            if( millis() >= ourTime + 175 ){
                position = 3;
            }
            break;
        case 3:
            digitalWrite( ledPin, LOW );
            ourTime = millis();
            position = 4;
            break;
        case 4:
            if( millis() >= ourTime + 175 &&              // If we haven't finished all three "S" pulses
                letterCount != 3 )
                position = 1;
            else if( millis() >= ourTime + 875 &&         // If this is the second "S"
                     letterCount == 3 && 
                     round == 2 ){
                letterCount = 0;
                round = 1;
                position = 1;
            }
            else if( millis() >= ourTime + 400 &&         // If this is the first "S"
                     letterCount == 3 && 
                     round == 1 ){
                letterCount = 0;
                round++;
                position = 5;
            }
            break;

        // Begin letter "O"
        case 5:
            digitalWrite( ledPin, HIGH );
            ourTime = millis();
            letterCount++;
            position = 6;
            break;
        case 6:
            if( millis() >= ourTime + 325 )
                position = 7;
            break;
        case 7:
            digitalWrite( ledPin, LOW );
            ourTime = millis();
            position = 8;
            break;
        case 8:
            if( millis() >= ourTime + 325 &&              // If we haven't finished all three "O" pulses
                letterCount != 3 )
                position = 5;
            else if( millis() >= ourTime + 400 &&         // If We are done displaying "O", go back for second "S"
                     letterCount == 3 ){
                letterCount = 0;
                position = 1;
            }
            break;
    }
}
