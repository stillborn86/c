/*
20141215 - Created by stillborn86 (c)

This sketch is a smaller version of the first SOS sketch which was previously
created.

For testing purposes, this sketch uses the LED on Pin13 and doesn't utilize a
photoresistor in order to make debugging and developing easier.
*/

int ledPin = 13;  // Sets the pin # for the functioning LED
int wait = 0;  // Sets the wait and blinkNum variables
int blinkNum = 1;

void setup()
{
  pinMode (ledPin, OUTPUT);  // Sets the ledPin as an output
}

void loop()
{
  if (blinkNum >= 10 || blinkNum <= 0)  // If SOS has been completely "spelled"
                                        // out
  {
    delay(1000);  // Delays a second before beggining the next cycle
    blinkNum = 1;  // Sets the blink counter back to the first blink
  }
  
  if (blinkNum == 1 || blinkNum == 2 || blinkNum == 3 || blinkNum == 7 ||
  blinkNum == 8 || blinkNum == 9)  // If the blink counter indicates a "short"
                                   // blink
  {
    wait = 200;  // Sets the delay for a "short" blink on and off
  }
  else  // If the blink counter indicates anything other than a "short" blink
  {
    wait = 400;  // Sets the delay for a "long" blink on and off
  }
  
  digitalWrite(ledPin, HIGH);  // Turns the LED on
  delay(wait);  // Leaves LED on for either a "long" or "short" blink
  digitalWrite(ledPin,LOW);  // Turns the LED on
  delay(wait);  // Leaves LED off for either a "long" or "short" blink
  
  if (blinkNum == 3 || blinkNum == 6 || blinkNum == 9)  // If blinks are between letters
  {
    delay(500);  // Sets a half-second delay between "letters"
  }
  
  ++blinkNum;  // Incriments blink counter by one
}
