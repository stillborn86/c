/*
20141128 - Created by stillborn86 (c)

This sketch sets up random "dancing" LED's on GPIO 2, 3, 4, and 5 by turning
them on and off randomly.
*/

int pinOn;
int pinOn2;

void setup()
{
  pinMode(2, OUTPUT);  // Initialize pins as outputs
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

void loop()
{  
  // Pick a new pinON
  int pinOn = random(2,6);
  int pinOn2 = random(2,6);
  
  if (pinOn != pinOn2)
  {  
  // Turn pinON on
  digitalWrite(pinOn, HIGH);
  digitalWrite(pinOn2, HIGH);
  
  // Delay 1/2 seconds
  delay(150);
  
  // Turn pinOn off
  digitalWrite(pinOn, LOW);
  digitalWrite(pinOn2, LOW);
  }
}
