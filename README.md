# C & C++ sketch compilation
This directory is full of my C/C++ sketches.

These sketches are available for educational purposes only, and any damage
resulting from the use of any code found here is the sole responsibility of the
user.

Any sketches that invoke the use of GPIO or other hardware level items must be
run using the `sudo` command in order to access hardware level systems.  Without
this command, the GPIO/hardware will not function properly.

Any sketches which do not call upon GPIO can/will function properly without the
use of root level access.

## Arduino folder
The arduino folder consists of C sketches designed exclusively for the Arduino
platform.  As these sketches have esoteric calls for GPIO ports and other
hardware-level items, it is necessary to seperate these from general C sketches.

## Help folder
The help folder is filled with small sketch "bites" consisting of example code,
each doing different things.  These tiny sketches are examples of smaller code
demonstrating mathematical functions, tuples, and other esoteric bits for the C
language.

## Tutorial folder
The tutorial folder is full of sketches that were either designed by others or
created with the help of others.  It was designed to be used as a helpful tool
for writing sketches in the future.

## Image files
The images are references, such as standard, GPIO pinouts.

## Downloading

    cd ~/Temp/dev/
    git clone https://stillborn86@bitbucket.org/stillborn86/c.git

## Uploading

    git init
    git remote add origin https://stillborn86@bitbucket.org/stillborn86/c.git
    git add *
    git commit -m "current date"
    git push -u origin master
